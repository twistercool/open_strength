#include "open_strength.h"

const float ratio_lift_to_total_male[] = {
    0.34523775, 0.2761902,   0.396825, 0.396825,     0.222222,
    0.25793625, 0.211507725, -1,       0.1676585625, 0.2229858881,
    0.13412685, -1,          -1,       0.21031725};
const float ratio_lift_to_total_female[] = {
    0.34854792,   0.278838336,  0.414938, 0.414938,    0.23236528,
    0.23651466,   0.1939420212, -1,       0.153734529, 0.2044669236,
    0.1229876232, -1,           -1,       0.21991714};

/*
   Those coefficients are applied from ages 14 to 22, inclusive
*/
const double foster_coeff[9] = {1.23, 1.18, 1.13, 1.08, 1.06,
                                1.04, 1.03, 1.02, 1.01};

/*
   Those coefficients are applied from ages 41 to 90, inclusive
*/
const double mcCulloh_coeff[50] = {
    1.01,  1.02,  1.031, 1.043, 1.055, 1.068, 1.082, 1.097, 1.113, 1.13,
    1.147, 1.165, 1.184, 1.204, 1.225, 1.246, 1.268, 1.291, 1.315, 1.340,
    1.366, 1.393, 1.421, 1.45,  1.48,  1.511, 1.543, 1.576, 1.610, 1.645,
    1.681, 1.718, 1.756, 1.795, 1.835, 1.876, 1.918, 1.961, 2.005, 2.05,
    2.096, 2.143, 2.19,  2.238, 2.287, 2.337, 2.388, 2.44,  2.494, 2.549};

const char* getCategory(double score) {
   if (score < 30) return strength_categories[0];
   if (score < 45) return strength_categories[1];
   if (score < 60) return strength_categories[2];
   if (score < 75) return strength_categories[3];
   if (score < 87.5) return strength_categories[4];
   if (score < 100) return strength_categories[5];
   if (score < 112.5) return strength_categories[6];
   if (score < 125) return strength_categories[7];

   return strength_categories[8];
}

/*
   Returns the old wilks coefficient
*/
double getWilksCoeff(gender gend, double bw) {
   double a, b, c, d, e, f;

   if (gend == male) {
      a = -216.0475144;
      b = 16.2606339;
      c = -0.002388645;
      d = -0.00113732;
      e = 7.01863e-6;
      f = -1.291e-8;
   } else {
      a = 594.31747775582;
      b = -27.23842536447;
      c = 0.82112226871;
      d = -0.00930733913;
      e = 4.731582e-5;
      f = -9.054e-8;
   }

   return 500 / (a + b * bw + c * pow(bw, 2) + d * pow(bw, 3) + e * pow(bw, 4) +
                 f * pow(bw, 5));
}

double getWilks(double total, gender gend, double bw) {
   if (total < 0 || bw <= 0) {
      fprintf(stderr, "Incorrect inputs for getWilks: %f, %f\n", total, bw);
      return -1;
   }
   return total * getWilksCoeff(gend, bw);
}

double get1RM(double weight, unsigned short reps) {
   return (100 * weight) / (48.8 + 53.8 * exp(-0.075 * reps));
}

double getAgeCoeff(unsigned short age) {
   if (age < 14) age = 14;  // no data on below 14 y.o.
   if (age > 90) age = 90;  // no data on above 90 y.o.

   if (age < 23) {
      return foster_coeff[age - 14];
   }
   if (age > 40) {
      return mcCulloh_coeff[age - 41];
   } else
      return 1.0;
}

double getScoreFromTotal(double total, gender gend, double bw,
                         unsigned short age) {
   double score = getWilks(total, gend, bw) / 4.0;

   score *= getAgeCoeff(age);

   return score;
}

double getTotalFromScore(double score, gender gend, double bw,
                         unsigned short age) {
   return (score * 4.0) / (getAgeCoeff(age) * getWilksCoeff(gend, bw));
}

double getTotalFromLift(lift input_lift, double weight_lift, gender gend,
                        double bw) {
   double added_weight = (weight_lift - bw) *
                         2.204623;  // for some reason, converted to pounds...
   if (input_lift == dip) {
      double percent_total = (gend == male)
                                 ? 1.68064e-10 * pow(added_weight, 4) -
                                       1.2945e-7 * pow(added_weight, 3) +
                                       371905e-10 * pow(added_weight, 2) -
                                       0.00499168 * added_weight + 0.566576

                                 : 8.249e-10 * pow(added_weight, 4) -
                                       4.01956e-7 * pow(added_weight, 3) +
                                       622122e-10 * pow(added_weight, 2) -
                                       0.00431442 * added_weight + 0.37562;
      return weight_lift / percent_total;
   } else if (input_lift == chin_up || input_lift == pull_up) {
      double percent_total = (gend == male)
                                 ? 4.01897e-10 * pow(added_weight, 4) -
                                       2.34536e-7 * pow(added_weight, 3) +
                                       502252e-10 * pow(added_weight, 2) -
                                       0.00502633 * added_weight + 0.459545

                                 : 1.66589e-9 * pow(added_weight, 4) -
                                       5.1621e-7 * pow(added_weight, 3) +
                                       54088e-9 * pow(added_weight, 2) -
                                       0.00281674 * added_weight + 0.302005;

      return (input_lift == chin_up) ? weight_lift / percent_total
                                     : 0.95 * weight_lift / percent_total;
   }
   return (gend == male) ? weight_lift / ratio_lift_to_total_male[input_lift]
                         : weight_lift / ratio_lift_to_total_female[input_lift];
}

double getScoreOfLift(lift input_lift, double weight_lift, gender gend,
                      double bw, unsigned short age) {
   return getScoreFromTotal(getTotalFromLift(input_lift, weight_lift, gend, bw),
                            gend, bw, age);
}

double getLiftFromScore(double score, lift input_lift, gender gend, double bw,
                        unsigned short age) {
   score *= 4.0;

   score /= getAgeCoeff(age);
   // now the score is the expected wilks

   score /= getWilksCoeff(gend, bw);
   // now the score is the expected PL total

   if (input_lift == dip || input_lift == chin_up || input_lift == pull_up) {
      fprintf(stderr,
              "Can't get weight of lift from dips/chin-ups/pull-ups yet\n");
      return -1.0;
   }

   return (gend == male) ? score * ratio_lift_to_total_male[input_lift]
                         : score * ratio_lift_to_total_female[input_lift];
}
