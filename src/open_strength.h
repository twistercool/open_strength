#ifndef OPEN_STRENGTH_H_
#define OPEN_STRENGTH_H_

#include <ctype.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NB_LIFTS 14
#define NB_STRENGTH_CATEGORIES 9
#define NB_LIFTTYPES 5
#define NB_MUSCLE_GROUPS 22

typedef enum gender { male = 0, female = 1 } gender;

typedef enum lift {
   back_squat = 0,
   front_squat,
   conv_deadlift,
   sumo_deadlift,
   power_clean,
   bench_press,
   incline_bench_press,
   dip,
   ohp,
   push_press,
   snatch_press,
   chin_up,
   pull_up,
   pendlay_row
} lift;

static const char* lift_names[NB_LIFTS] = {
    "Back Squat",  "Front Squat", "Conv Deadlift", "Sumo Deadlift",
    "Power Clean", "Bench Press", "Incline Bench", "Dip",
    "Ohp",         "Push Press",  "Snatch Press",  "Chin Up",
    "Pull Up",     "Pendlay Row"};

static const char* strength_categories[NB_STRENGTH_CATEGORIES] = {
    "Subpar",   "Untrained",   "Novice", "Intermediate", "Proficient",
    "Advanced", "Exceptional", "Elite",  "World Class"};

typedef enum liftType {
   squat = 0,
   floor_pull,
   h_press,
   v_press,
   pull_row
} liftType;

static const char* liftType_names[NB_LIFTTYPES] = {
    "Squat", "Floor Pull", "Horizontal Press", "Vertical Press", "Pull Up/Row"};

static const liftType liftType_of_lift[NB_LIFTS] = {
    squat,   squat,   floor_pull, floor_pull, floor_pull, h_press,  h_press,
    h_press, v_press, v_press,    v_press,    pull_row,   pull_row, pull_row};

typedef struct liftInput {
   lift lift_performed;
   int weight;
   int reps;
   int rir;
} liftInput;

static const char* muscle_group_names[NB_MUSCLE_GROUPS] = {
    "Upper Traps",
    "Middle Traps",
    "Lower Traps",
    "Anterior Delts",
    "Lateral Delts",
    "Posterior Delts",
    "Rotator Cuffs",
    "Pecs (Clavicular)",
    "Pecs (Sternal)",
    "Biceps",
    "Triceps",
    "Forearms",
    "Obliques And Serratus",
    "Abdominals",
    "Lats",
    "Spinal Erectors",
    "Glutes",
    "Hamstrings",
    "Quads",
    "Hip Flexors",
    "Hip Adductors",
    "Calves"};

enum muscle_groups {
   upper_traps = 0,
   middle_traps,
   lower_traps,
   anterior_delts,
   lateral_delts,
   posterior_delts,
   rotator_cuffs,
   pecs_clavicular,
   pecs_sternal,
   biceps,
   triceps,
   forearms,
   obliques_and_serratus,
   abdominals,
   lats,
   spinal_erectors,
   glutes,
   hamstrings,
   quads,
   hip_flexors,
   hip_adductors,
   calves
};

/*
   FUNCTION PROTOTYPES
*/
extern const char* getCategory(double score);

extern double getWilksCoeff(gender gend, double bw);

extern double getWilks(double total, gender gend, double bw);

extern double getLiftFromScore(double score, lift in_lift, gender gend,
                               double bw, unsigned short age);

extern double getScoreOfLift(lift input_lift, double weight_lift, gender gend,
                             double bw, unsigned short age);

extern double getScoreForLiftType(liftType input_liftType, lift input_lifts[],
                                  double input_1RMs[], size_t nb_inputs,
                                  gender gend, double bw, unsigned short age);

extern double getScoreFromTotal(double total, gender gend, double bw,
                                unsigned short age);

extern double getOverallScore(lift input_lifts[], double input_weights[],
                              size_t nb_inputs, gender gend, double bw,
                              unsigned short age);

extern double getSymmetryScore(double lift_scores[], size_t nb_lifts);

extern double getTotalFromLift(lift input_lift, double weight_lift, gender gend,
                               double bw);

extern double getTotalFromScore(double score, gender gend, double bw,
                                unsigned short age);

extern double get1RM(double weight, unsigned short reps);

double* getScoreForAllMuscleGroups(lift lifts_selected[], double lifts_1RMs[],
                                   size_t nb_inputs, gender gend, double bw,
                                   unsigned short age);
#endif
