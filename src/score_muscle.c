#include "open_strength.h"

/*
   Array of arrays of muscle involvement scores for each muscle group for each
   lift.

   The muscle groups are ordered in the same order of the enum "muscle_groups".
   The lifts are order in the same order as the enum "lift'.

   Example: for the muscle involvement of lats on a pendlay_row,
      access muscle_involvement_per_lift[pendlay_row][lats]
*/
const unsigned char muscle_involvement_per_lift[NB_LIFTS][NB_MUSCLE_GROUPS] = {
    // back_squat
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 6, 2, 6, 9, 6, 8, 4, 6, 2},
    // front_squat
    {2, 2, 2, 0, 0, 0, 2, 0, 0, 0, 0, 0, 2, 8, 0, 4, 7, 4, 10, 4, 6, 2},
    // conv_deadlift
    {8, 8, 2, 0, 0, 0, 0, 0, 0, 0, 0, 4, 4, 6, 4, 10, 7, 7, 6, 2, 4, 2},
    // sumo_deadalift
    {8, 8, 2, 0, 0, 0, 0, 0, 0, 0, 0, 4, 4, 6, 4, 6, 8, 8, 8, 4, 6, 2},
    // power_clean
    {8, 8, 2, 0, 0, 0, 0, 0, 0, 0, 0, 6, 4, 6, 2, 8, 6, 6, 8, 2, 4, 3},
    // bench_press
    {0, 0, 0, 6, 0, 0, 2, 8, 10, 2, 8, 2, 0, 2, 4, 2, 0, 0, 2, 0, 0, 0},
    // incline_bench_press
    {0, 0, 0, 6, 0, 0, 2, 10, 8, 2, 8, 2, 0, 2, 4, 2, 0, 0, 2, 0, 0, 0},
    // dip
    {0, 0, 6, 6, 2, 0, 2, 6, 10, 0, 8, 2, 2, 0, 2, 0, 0, 0, 0, 0, 0, 0},
    // ohp
    {4, 4, 4, 10, 6, 0, 2, 4, 0, 2, 8, 2, 2, 4, 0, 2, 2, 0, 0, 0, 0, 0},
    // push_press
    {4, 4, 4, 8, 6, 0, 2, 2, 0, 2, 8, 2, 2, 4, 0, 2, 4, 2, 4, 2, 6, 3},
    // snatch_press
    {6, 6, 6, 8, 8, 2, 2, 0, 0, 2, 6, 2, 2, 4, 0, 2, 2, 0, 0, 0, 0, 0},
    // chin_up
    {0, 6, 6, 0, 0, 6, 6, 0, 0, 6, 0, 6, 4, 6, 10, 0, 0, 0, 0, 0, 0, 0},
    // pull_up
    {0, 4, 4, 0, 0, 6, 6, 2, 2, 8, 0, 4, 4, 8, 10, 0, 0, 0, 0, 0, 0, 0},
    // pendlay_row
    {2, 6, 6, 0, 0, 8, 8, 0, 2, 6, 0, 4, 4, 4, 10, 5, 3, 3, 0, 0, 2, 2}};

// gets a list of liftInputs and necessary info to calculate the scores of the
// lifts and calculates the score of each muscle group
// returned pointer needs to be freed after use
double* getScoreForAllMuscleGroups(lift lifts_selected[], double lifts_1RMs[],
                                   size_t nb_inputs, gender gend, double bw,
                                   unsigned short age) {
   double lift_score_arr[nb_inputs];
   for (int i = 0; i < (int)nb_inputs; i++) {
      lift_score_arr[i] =
          getScoreOfLift(lifts_selected[i], lifts_1RMs[i], gend, bw, age);
   }

   double* scores_muscles_arr =
       (double*)malloc(NB_MUSCLE_GROUPS * sizeof(double));

   // for each muscle group
   for (int i = 0; i < NB_MUSCLE_GROUPS; i++) {
      int numerator = 0;
      int denominator = 0;

      // for each lift
      for (int j = 0; j < (int)nb_inputs; j++) {
         int coefficient =
             pow(muscle_involvement_per_lift[lifts_selected[j]][i], 3);
         numerator += lift_score_arr[j] * coefficient;
         denominator += coefficient;
      }

      if (denominator == 0)
         scores_muscles_arr[i] = 0;
      else
         scores_muscles_arr[i] = (1.0 * numerator) / denominator;
   }

   return scores_muscles_arr;
}

double getScoreForLiftType(liftType input_lift_type, lift input_lifts[],
                           double input_1RMs[], size_t nb_inputs, gender gend,
                           double bw, unsigned short age) {
   double score = -1;

   for (int i = 0; i < (int)nb_inputs; i++) {
      double cur_score =
          getScoreOfLift(input_lifts[i], input_1RMs[i], gend, bw, age);
      liftType cur_type = liftType_of_lift[input_lifts[i]];
      if (cur_type == input_lift_type) {
         score = fmax(cur_score, score);
      }
   }

   return score;
}

double getOverallScore(lift in_lifts[], double input_1RMs[], size_t nb_inputs,
                       gender gend, double bw, unsigned short age) {
   double squat_score = getScoreForLiftType(squat, in_lifts, input_1RMs,
                                            nb_inputs, gend, bw, age);
   double floor_pull_score = getScoreForLiftType(
       floor_pull, in_lifts, input_1RMs, nb_inputs, gend, bw, age);
   double h_press_score = getScoreForLiftType(h_press, in_lifts, input_1RMs,
                                              nb_inputs, gend, bw, age);
   double v_press_score = getScoreForLiftType(v_press, in_lifts, input_1RMs,
                                              nb_inputs, gend, bw, age);
   double pull_row_score = getScoreForLiftType(pull_row, in_lifts, input_1RMs,
                                               nb_inputs, gend, bw, age);

   double scores_liftTypes[] = {squat_score, floor_pull_score, h_press_score,
                                v_press_score, pull_row_score};

   int liftTypes_inputed = 0;
   double sum = 0;
   for (int i = 0; i < 5; i++) {
      if (scores_liftTypes[i] != -1) {
         liftTypes_inputed++;
         sum += scores_liftTypes[i];
      }
   }

   return (nb_inputs != 0) ? sum / liftTypes_inputed : -1;
}

double getSymmetryScore(double lift_scores[], size_t nb_lifts) {
   double avg_score = 0;
   for (int i = 0; i < (int)nb_lifts; i++) avg_score += lift_scores[i];
   avg_score /= nb_lifts;

   double avg_variance = 0;
   for (int i = 0; i < (int)nb_lifts; i++)
      avg_variance += pow(lift_scores[i] - avg_score, 2);
   avg_variance /= nb_lifts;

   return 100 - avg_variance;
}
