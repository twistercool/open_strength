VERSION = 0.1
INCS = -I ../include
LIBS = -lm
TEST_LIBS = -lm -lopen_strength
CFLAGS += -std=c17 ${INCS} -DVERSION=\"${VERSION}\" -Ofast -fpic -Wall -Wextra -Wno-unused-parameter -Wno-unused-variable -Wno-unused-but-set-variable
LDFLAGS += ${LIBS}
TEST_CFLAGS += -std=c17 ${INCS} -DVERSION=\"${VERSION}\" -O0 -g -ggdb -Wall -Wextra -Wno-unused-parameter -Wno-unused-variable -Wno-unused-but-set-variable
TEST_LDFLAGS += ${TEST_LIBS}
CC = gcc
