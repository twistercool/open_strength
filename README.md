SymmetricStrength-inspired project to calculate the stregth score of lifts.

The format of the project is a liibray, which implements functions to get required information.


The lift indices are the following:
```
0  - conv_dl
1  - flat_bench_press
2  - back_squat
3  - sumo_dl
4  - front_squat
5  - power_clean
6  - incline_bench_press
7  - dip
8  - ohp
9  - push_press
10 - snatch_press
11 - chin_up
12 - pull_up
13 - pendlay_row
```


# Installation

1. cd into the build directory
2. sudo make install
