#include <assert.h>

#include "../src/open_strength.h"

void testWilks();
void testLiftFromScore();
void testScoreOfLift();
void testScoreForLiftType();
void testgetOverallScore();

int main() {
   testWilks();
   testLiftFromScore();
   testScoreOfLift();
   testScoreForLiftType();
   testgetOverallScore();

   printf("All tests passed!\n");
}

bool isAlmostZeroF(double diff) {
   if (fabs(diff) > 0.1) {
      fprintf(stderr, "The actual difference was: %f\n", diff);
      return false;
   } else
      return true;
}

void testWilks() {
   assert(isAlmostZeroF(getWilks(600.0, male, 45.0) - 691.88));
   assert(isAlmostZeroF(getWilks(600.0, female, 45.0) - 832.07));

   assert(isAlmostZeroF(getWilks(715.0, male, 120.0) - 411.07));
   assert(isAlmostZeroF(getWilks(715.0, female, 120.0) - 571.75));

   // should produce output on stderr
   assert(isAlmostZeroF(getWilks(-1, male, -1) - -1));
   assert(isAlmostZeroF(getWilks(-1, female, -1) - -1));
}

void testLiftFromScore() {
   assert(
       isAlmostZeroF(getLiftFromScore(30, back_squat, male, 100.0, 20) - 66.1));
   assert(
       isAlmostZeroF(getLiftFromScore(45, back_squat, male, 100.0, 20) - 99.2));
}

void testScoreOfLift() {
   assert(
       isAlmostZeroF(getScoreOfLift(back_squat, 66.1, male, 100.0, 20) - 30));
   assert(
       isAlmostZeroF(getScoreOfLift(back_squat, 99.2, male, 100.0, 20) - 45));
}

void testScoreForLiftType() {
#define NB_INPUTS 4
   lift input_lifts[NB_INPUTS] = {back_squat, front_squat, conv_deadlift,
                                  bench_press};

   double input_1RMs[NB_INPUTS] = {180, 120, 220, 120};
   double bw = 100;
   unsigned short age = 20;

   assert(isAlmostZeroF(getScoreForLiftType(squat, input_lifts, input_1RMs,
                                            NB_INPUTS, male, bw, age) -
                        fmax(getScoreOfLift(back_squat, 180, male, bw, age),
                             getScoreOfLift(front_squat, 120, male, bw, age))));
   assert(isAlmostZeroF(getScoreForLiftType(floor_pull, input_lifts, input_1RMs,
                                            NB_INPUTS, male, bw, age) -
                        getScoreOfLift(conv_deadlift, 220, male, bw, age)));

   assert(isAlmostZeroF(getScoreForLiftType(h_press, input_lifts, input_1RMs,
                                            NB_INPUTS, male, bw, age) -
                        getScoreOfLift(bench_press, 120, male, bw, age)));

   assert(isAlmostZeroF(getScoreForLiftType(v_press, input_lifts, input_1RMs,
                                            NB_INPUTS, male, bw, age) -
                        -1));
   assert(isAlmostZeroF(getScoreForLiftType(pull_row, input_lifts, input_1RMs,
                                            NB_INPUTS, male, bw, age) -
                        -1));
}

void testgetOverallScore() {
#define NB_INPUTS 4
   lift input_lifts[NB_INPUTS] = {back_squat, front_squat, conv_deadlift,
                                  bench_press};

   double input_1RMs[NB_INPUTS] = {180, 120, 220, 120};
   double bw = 100;
   unsigned short age = 20;

   assert(isAlmostZeroF(
       getOverallScore(input_lifts, input_1RMs, NB_INPUTS, male, bw, age) -
       81.1));

   lift input_lifts_2[] = {back_squat, conv_deadlift, bench_press};
   double input_1RMs_2[] = {182.347543, 222.869219, 121.565029};
   assert(isAlmostZeroF(
       getOverallScore(input_lifts_2, input_1RMs_2, 3, male, 89, 20) - 79.9));
}
